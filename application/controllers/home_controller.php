<?php

class home_controller extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
 }
 
	function index()
	{
		if($this->session->userdata('login'))
		{
			$session_data = $this->session->userdata('login');
			$data['pictureUrl'] = $session_data['pictureUrl'];
			$data['name'] = $session_data['name'];
			$this->load->view('login/home_view', $data);
		}
		else{
			redirect(site_url().'/login_controller', 'refresh');
		}
	}
	function logout()
	{
		session_destroy();
		$this->session->unset_userdata('login');
		redirect('login_controller', 'refresh');
	}
}
?>