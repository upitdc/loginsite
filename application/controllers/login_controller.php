<?php

	class Login_Controller extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('login_model');	
		}
			
		public function index(){
			if($this->session->userdata('login')){
				redirect(site_url().'/home_controller', 'refresh');
			}
			else{
				$actionperform = $this->input->post('loginwith');
				if($actionperform=='facebook'){
					$this->facebook();
				}
				else if($actionperform=='google'){
					$this->google();
				}
				else{
					$this->load->view('login/guest_view');	
				}
			}
		}
		public function facebook(){
				$provider = new \League\OAuth2\Client\Provider\Facebook([
					'clientId'          => '914706291992152',
					'clientSecret'      => 'aa795826d3c68ed5360100696c3fdaa4',
					'redirectUri'       => site_url().'/login_controller/facebook',
					'graphApiVersion'   => 'v2.6',
				]);
					
				if (!isset($_GET['code'])) {
			
					// If we don't have an authorization code then get one
					$authUrl = $provider->getAuthorizationUrl([
						'scope' => ['email', 'public_profile', 'user_friends'],
					]);
					$_SESSION['oauth2state'] = $provider->getState();
					
					redirect($authUrl, 'refresh');
					exit;

				// Check given state against previously stored one to mitigate CSRF attack
				} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

					unset($_SESSION['oauth2state']);
					echo 'Invalid state.';
					exit;

				}

				// Try to get an access token (using the authorization code grant)
				$token = $provider->getAccessToken('authorization_code', [
					'code' => $_GET['code']
				]);
				$user = $provider->getResourceOwner($token);
				// Use these details to create a new profile
				$userData = array(
					 'pictureUrl' => $user->getPictureUrl(),
					 'name' => $user->getName(),
				);
				$this->session->set_userdata('login', $userData);
				redirect(site_url().'/home_controller', 'refresh');
		}
		public function google(){
			$provider = new League\OAuth2\Client\Provider\Google([
				'clientId'     => '769434087037-824usr5147h5id0nagv538s8jeb0t8n6.apps.googleusercontent.com',
				'clientSecret' => 'Apz3o1PfXGILTA_sZgjc7YI4',
				'redirectUri'  => site_url().'/login_controller/google',
				'hostedDomain' => 'http://localhost/',
			]);

			if (!empty($_GET['error'])) {

				// Got an error, probably user denied access
				exit('Got error: ' . $_GET['error']);

			} elseif (empty($_GET['code'])) {

				// If we don't have an authorization code then get one
				$authUrl = $provider->getAuthorizationUrl();
				$_SESSION['oauth2state'] = $provider->getState();
				header('Location: ' . $authUrl);
				exit;

			} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

				// State is invalid, possible CSRF attack in progress
				unset($_SESSION['oauth2state']);
				exit('Invalid state');

			} else {

				// Try to get an access token (using the authorization code grant)
				$token = $provider->getAccessToken('authorization_code', [
					'code' => $_GET['code']
				]);

				// Optional: Now you have a token you can look up a users profile data
				$user = $provider->getResourceOwner($token);
				// Use these details to create a new profile
				$userData['pictureUrl'] = $user->getAvatar();
				$userData['name'] = $user->getName();
				$this->session->set_userdata('login', $userData);
				redirect(site_url().'/home_controller', 'refresh');
			}
		}
		
	}
?>