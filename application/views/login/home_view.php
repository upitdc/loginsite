<html>
<head>
	<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<title>Profile</title>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		body{background: url("http://i.imgur.com/eWtfMME.png") no-repeat center center fixed;
        -webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		}
		div.opaque{
			position: fixed;
			width:100%;
			height:100vh;
			background: rgba(25, 25, 25, .5);
			opacity:0.7;
			top:0;
			left:0;
		}
		div.content{
			position: absolute;
			width:100%;
			height:100%;
		}
		div.information{
			position:relative;
			color:white;
			text-align:center;
		}
		#detail{
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 2s; /* Chrome, Safari, Opera */
			animation-name: the_content;
			animation-duration: 2s;

		}
		@-webkit-keyframes the_content {
			0%  {background-color:transparent;opacity:0.0}
			33%  {background-color:transparent;opacity:0.0}
			100% {background-color:transparent;opacity:1.0}
		}
		#image_border{
			position: relative;
			width:290px;
			height: 290px;
			margin-left:calc((100% - 290px)/2);
			border: 6px solid white;
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 1s; /* Chrome, Safari, Opera */
			animation-name: example;
			animation-duration: 1s;
		}
		@-webkit-keyframes example {
		0%   {background-color:transparent;opacity:0.0;margin-left:0px;}
		100%  {background-color:transparent;opacity:1.0;margin-left:calc((100% - 290px)/2);}
		}
		#image{
			position: relative;
			width:250px;
			height: 250px;
			margin-top:calc((100% - 250px)/2);
			margin-left:calc((100% - 250px)/2);
		}
		#logoutbutton{
			position:relative;
			background-color:transparent;
			border-radius: 24px;
			color:#ccccff;
			border: 3px solid #ccccff;
			width:280px;
			font-size:20px;
			margin-left:calc((100% - 280px)/2);
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 3s; /* Chrome, Safari, Opera */
			animation-name: the_button;
			animation-duration: 3s;
		}
		@-webkit-keyframes the_button {
			0%   {background-color:transparent;opacity:0.0}
			50%   {background-color:transparent;opacity:0.0}
			100%  {background-color:transparent;opacity:1}
		}
		h1{
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 1s; /* Chrome, Safari, Opera */
			animation-name: title;
			animation-duration: 1s;
		}
		@-webkit-keyframes title {
			0%   {background-color:transparent;opacity:0.0;}
			100%  {background-color:transparent;opacity:1.0;}
		}
		div.jumbotron{
			position:relative;
			background-color:transparent;
		}
		div.container{
			margin-top:80px;
		}
	</style>
</head>
<body>
	<div class="opaque"></div>
	<div class="content">
		<div class="container-fluid	">
			<div class="row" style="margin-top:15%">
				<div class="opaque"></div>
				<div class="col-sm-12">
					<h1 style="color:white;font-family: 'Montserrat', sans-serif;font-size:50px;text-align:center">PROFILE</h1>
				</div>
				<br><br>
				<div class="col-sm-2">
				</div>
				<div class="col-sm-3">
					<div id="image_border" class="img-circle">
						<img id="image" class="img-circle" src="<?php echo $pictureUrl?>" alt="Profile Picture">
					</div>
				</div>
				<div class="col-sm-1">
				</div>
				<div class="col-sm-4">
					<div class="information" id="detail">
						<br>
						<h2 style="font-family: 'Muli', sans-serif;">Welcome, <p style="color:#ccccff"><?php echo $name?></p></h2>
					</div>
					<br><br>
					<a type="button" id="logoutbutton" class="btn btn-primary" href="<?php echo site_url('home_controller/logout')?>">LOG-OUT</a>
				</div>
				<div class="col-sm-2">
				</div>
				
			</div>
		</div>
	</div>
</body>
</html>