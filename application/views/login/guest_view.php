<html>
<head>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<title>Welcome</title>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		body{background: url("http://www.hdwallpapers.in/walls/dramatic_sky-wide.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		}
		div.opaque{
			position: fixed;
			width:100%;
			height:100vh;
			background: rgba(25, 25, 25, .9);
			opacity:0.7;
			top:0;
			left:0;
		}
		div.content{
			position: relative;
			height:400px;
			width:300px;
			margin-left:calc((100% - 300px)/2);
			border: 3px solid white;
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 3s; /* Chrome, Safari, Opera */
			animation-name: the_content;
			animation-duration: 3s;
		}
		@-webkit-keyframes the_content {
		0%  {background-color:transparent;opacity:0.0}
		33%  {background-color:transparent;opacity:0.0}
		100% {background-color:transparent;opacity:1.0}
		}
		div.the_title{
			position:relative;
			color:white;
			right:0;
			-webkit-animation-name: example; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 2s; /* Chrome, Safari, Opera */
			animation-name: example;
			animation-duration: 2s;
		}		
		@-webkit-keyframes example {
		0%   {background-color:transparent;opacity:0.0}
		100%  {background-color:transparent;opacity:1}
		}
		div.jumbotron{
			position:relative;
			background-color:transparent;
		}
		#title{
			font-family: 'Montserrat', sans-serif;
			font-size: 48px;
			color:white;
			text-align:center;
		}
		#fbutton{
			font-family: 'Raleway', sans-serif;
			position:relative;
			background-color:#3b5998;
			border-radius: 24px;color:white;
			border: 3px solid #3b5998;
			width:250px;
			font-size:20px;
			margin-left:calc((100% - 250px)/2);
		}
		#gbutton{
			font-family: 'Raleway', sans-serif;
			position:relative;
			background-color:#e53935;
			border-radius: 24px;color:white;
			border: 3px solid #e53935;
			width:250px;
			font-size:20px;
			margin-left:calc((100% - 250px)/2);
		}
	</style>
</head>
<body>
	<div class="container-fluid	">
		<div class="row" style="margin-top:15%">
			<div class="opaque"></div>
			<div class="col-sm-1">
			</div>
			<div class="col-sm-6">
				<div class="the_title">
					<div class="jumbotron">
						<h1 style="text-align:center;font-family: 'Ubuntu', sans-serif;">Log in to your favorite website.</h1>
						<h2 style="text-align:center;font-family: 'Ubuntu', sans-serif;">Anytime. Anywhere.</h2>
						<br><br><br>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content">
					<form method="POST">
					<br>
					<h2 id="title">Logify</h2>
					<br><br>
					<button id="fbutton" type="submit" class="btn btn-primary" value="facebook" name="loginwith">Log-in Using Facebook</button>
					<br><br>
					<button id="gbutton" type="submit" class="btn btn-primary" value="google" name="loginwith">Log-in Using Gmail</button>
					<br><br><br>
					<h6 style="font-family: 'Open Sans', sans-serif;color:white;text-align:center">By signing up, you agree to Logify's Terms and Conditions of Use and Privacy Policy</h6>
					</form>
				</div>
			</div>
			<div class="col-sm-2">
			</div>
			
		</div>
	</div>
</body>
</html>