<?php

	require_once APPPATH.'vendor\autoload.php';
	require_once APPPATH.'libraries\facebook\src\Provider\Facebook.php';
	
	class login_model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
			$this->load->library('session');
		}
		
		function google(){
			$provider = new League\OAuth2\Client\Provider\Google([
				'clientId'     => '769434087037-824usr5147h5id0nagv538s8jeb0t8n6.apps.googleusercontent.com',
				'clientSecret' => 'Apz3o1PfXGILTA_sZgjc7YI4',
				'redirectUri'  => 'http://localhost/loginsite/index.php/home_controller/',
				'hostedDomain' => 'http://localhost/',
			]);

			if (!empty($_GET['error'])) {

				// Got an error, probably user denied access
				exit('Got error: ' . $_GET['error']);

			} elseif (empty($_GET['code'])) {

				// If we don't have an authorization code then get one
				$authUrl = $provider->getAuthorizationUrl();
				$_SESSION['oauth2state'] = $provider->getState();
				header('Location: ' . $authUrl);
				exit;

			} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

				// State is invalid, possible CSRF attack in progress
				unset($_SESSION['oauth2state']);
				exit('Invalid state');

			} else {

				// Try to get an access token (using the authorization code grant)
				$token = $provider->getAccessToken('authorization_code', [
					'code' => $_GET['code']
				]);

				// Optional: Now you have a token you can look up a users profile data
				try {

					// We got an access token, let's now get the owner details
					$user = $provider->getResourceOwner($token);
					// Use these details to create a new profile
					return $user;
					} catch (Exception $e) {
					// Failed to get user details
					exit('Something went wrong: ' . $e->getMessage());
				}
			}
		}
	}
?>